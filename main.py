from functions import m_json
from functions import m_pck

# Pfade zu Dateien
path_1 = "/home/pi/calorimetry_home/datasheets/setup_heat_capacity.json"
path_2 = "/home/pi/calorimetry_home/datasheets/setup_newton.json"
folder_path = "/home/pi/calorimetry_home/datasheets"
data_folder_1 = "/home/pi/calorimetry_home/data/heat_capazity"
data_folder_2 = "/home/pi/calorimetry_home/data/newton"
json_folder = "/home/pi/calorimetry_home/datasheets"

# Liest alle UUIDs einer setup-File aus und sortiert diese nach type
metadata = m_json.get_metadata_from_setup(path_2)
print(metadata)

# Liest die Serials der Sensoren aus und ergänzt sie im Metadaten-Dictionary
m_json.add_temperature_sensor_serials(folder_path, metadata)

# Daten archivieren
m_json.archiv_json(folder_path,path_2,data_folder_2)

# Nimmt Messungen vor, die durch ENTER gestartet und mit STRG+C beendet werden
data = m_pck.get_meas_data_calorimetry(metadata)

# Erstellt ein H5-File, in welchem die Messdaten abgelegt werden
m_pck.logging_calorimetry(data,metadata,data_folder_2,json_folder)

